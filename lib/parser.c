/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include <string.h>
#include <stdlib.h>

#include "parser.h"

void parse_command(const char* input,
                   int* argc, char*** argv)
{
  // TODO: Fill it!
  int count = 0;
  char str[100];
  strcpy(str, input);

  char* sArr[10];
  char* ptr = strtok(str, " \n");
  int len;

  if(ptr == NULL)
  {
    *argv = (char**)malloc(sizeof(char*)*2);
    *(*argv + 0) = (char*)malloc(sizeof(char)*(strlen(str)+1));
    strcpy(*(*argv+0), str);
    *(*argv+1) = NULL;
    *argc = 1;
  }
  else
  {
    while(ptr != NULL)
    {
      len = strlen(ptr) + 1;
      sArr[count] = (char*)malloc(sizeof(char)*len);
      strcpy(sArr[count++],ptr);
      if(strchr(ptr, '\"') != NULL)
      {
        strcpy(sArr[count-1], ptr+1);
        ptr = strtok(NULL, "\"");
        sArr[count-1] = (char*)realloc(sArr[count-1], sizeof(char)*(len+strlen(ptr)));
        strcat(sArr[count-1], " ");
        strcat(sArr[count-1], ptr);
        break;
      }
      ptr = strtok(NULL, " \n");
    }
    *argc = count;
    *argv = (char**)malloc(sizeof(char*)*(count+1));

    for(count = 0; count<*argc; count++)
    {
      *(*argv+count) = (char*)malloc(sizeof(char)*(strlen(sArr[count])+1));
      strcpy(*(*argv+count), sArr[count]);
      free(sArr[count]);
    }
    *(*argv+count) = NULL;
  }
}

int parse_background(int* argc, char** argv)
{
  if(strcmp(argv[(*argc)-1], "&") == 0)
  {
    argv[(*argc)-1] = NULL;
    return 1;
  }

  return 0;
}
