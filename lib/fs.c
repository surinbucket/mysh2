/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "fs.h"

int does_exefile_exists(const char* path)
{
  // TODO: Fill it!
  if(access(path, X_OK) == 0)
    return 1;
  else
    return 0;
}

int resolve_path(char* argv)
{
  char* path_val[6] = {"/usr/local/sbin/", "/usr/local/bin", "/usr/sbin/", "/usr/bin/", "/sbin/", "/bin/"};
  char* path[6];
  if(does_exefile_exists(argv)) return 1;

  int i;
  for(i=5; i>=0; i--)
  {
    path[i] = (char*)malloc(strlen(path_val[i]) + strlen(argv));
    strncpy(path[i], path_val[i], strlen(path_val[i]));
    strncat(path[i], argv, strlen(argv));

    if(does_exefile_exists(path[i]))
    {
      strncpy(argv, path[i], strlen(path[i]));
      free(path[i]);
      return 1;
    }
  }
  for(i=0; i<6; i++)  free(path[i]);
  return 0;
}