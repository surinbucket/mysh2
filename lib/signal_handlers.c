#include <sys/types.h>
#include <sys/wait.h>

#include <signal.h>
#include <stdio.h>

#include "signal_handlers.h"

void catch_SIGINT(int signo)
{
	signal(SIGINT, SIG_IGN);
	printf("\n");
	signal(SIGINT, catch_SIGINT);
}

void catch_SIGTSTP(int signo)
{
	signal(SIGTSTP, SIG_IGN);
	printf("\n");
	signal(SIGTSTP, catch_SIGTSTP);
}

void catch_zombie(int signo)
{
	pid_t pid;
	int status;
	pid = waitpid(-1, &status, WNOHANG);
	
	if(pid > 0)
		fprintf(stderr, "zombie alert\n");
}