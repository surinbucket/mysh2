/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#define _POSIX_SOURCE

#include "commands.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

static struct command_entry commands[] =
{
  {
    "pwd",
    do_pwd,
    err_pwd
  },
  {
    "cd",
    do_cd,
    err_cd
  },
  {
    "kill",
    signal_generators,
    err_kill
  },
  {
    "fg",
    do_fg,
    err_fg
  }
};

struct command_entry* fetch_command(const char* command_name)
{
  // TODO: Fill it.
  if(strcmp(command_name, "pwd") == 0)
    return &commands[0];
  else if(strcmp(command_name, "cd") == 0)
    return &commands[1];
  else if(strcmp(command_name, "kill") == 0)
    return &commands[2];
  else if(strcmp(command_name, "fg") == 0)
    return &commands[3];
  else 
    return NULL;
}

int do_pwd(int argc, char** argv)
{
  // TODO: Fill it.
  char path[1024];
  char* ptr = getcwd(path, 1024);
  if(ptr != NULL) 
  {
    printf("%s\n",path);
    fflush(stdout);
    return 0;
  }
  else  
    return -1;
}

void err_pwd(int err_code)
{
  //
}

int do_cd(int argc, char** argv)
{
  // TODO: Fill it.
  struct stat sb;
  stat(argv[1], &sb);
  int result = chdir(argv[1]);

  if(access(argv[1], F_OK) != 0)
  {
    return 1;
  }
  if(S_ISDIR(sb.st_mode) != 1)
    return 2;
  return 0;
}

void err_cd(int err_code)
{
  // TODO: Fill it.
  switch(err_code)
  {
    case 1:
      fprintf(stderr, "cd: no such file or directory\n");
      break;
    case 2:
      fprintf(stderr, "cd: not a directory\n");
      break;
    default:
      break;
  }
}

int signal_generators(int argc, char** argv)
{
  kill(atoi(argv[1]),SIGKILL);
}

void err_kill(int err_code)
{
  //
}

int do_fg(int argc, char** argv)
{
  int pid;
  int status;

  pid = waitpid(0, &status, WNOHANG);

  if(pid == 0)
    waitpid(BG.pid, &status, 0);
}

void err_fg(int err_code)
{
  //
}