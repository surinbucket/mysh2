/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include "parser.h"
#include "utils.h"
#include "fs.h"
#include "signal_handlers.h"

#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

int main(int argc, char** argv)
{ 
  signal(SIGINT, catch_SIGINT);
  signal(SIGTSTP, catch_SIGTSTP);
  signal(SIGCHLD, catch_zombie);

  int flag = 1;

  while(flag)
  {
    char command_buffer[4096] = { 0, };
    
    while (fgets(command_buffer, 4096, stdin) != NULL) {
      int argc = -1;
      char** argv = NULL;

      parse_command(command_buffer, &argc, &argv);

      BG.IS_BG = parse_background(&argc, argv);

      assert(argv != NULL);
      if (strcmp(argv[0], "exit") == 0) {
        FREE_2D_ARRAY(argc, argv);
        flag = 0;
        break;
      }
      struct command_entry* comm_entry = fetch_command(argv[0]);

      if (comm_entry != NULL) {
        int ret = comm_entry->command_func(argc, argv);
        if (ret != 0) {
          comm_entry->err(ret);
        }
      } else if (resolve_path(argv[0])) {
        // TODO: Execute the program of argv[0].

        pid_t pid; 
        pid = fork();

        if(pid < 0)
          printf("FORK FAILED\n");
        else if(pid == 0)
        {
          execv(argv[0], argv);
          exit(0);
        }
        else
        {
          if(BG.IS_BG)
          {
            BG.pid = pid;
            break;
          }
          wait((int*)0);
        }
      } else {
      assert(comm_entry == NULL);
      fprintf(stderr, "%s: command not found.\n", argv[0]);
      }
      FREE_2D_ARRAY(argc, argv);
    }
  }
  return 0;
}
