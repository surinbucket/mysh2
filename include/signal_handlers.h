#ifndef HANDLERS_H_
#define HANDLERS_H_

void catch_SIGINT(int signo);
void catch_SIGTSTP(int signo);
void catch_zombie(int signo);

#endif // HANDLERS_H_
